var msgDbRef;
var selectedHead = 1;
var selectedSubject = "";
var yourName = "";
var blinkID = "USER2";



$(document).ready(function(){

    //setBlinkId() 可以更換ID, 這行就可以移除了
    blinkID = window.location.search.split('?')[1];


    var config = {
        apiKey: "AIzaSyAGAdDpQWt52bV6_PUgpp55aawYN0k9Jyg",
        authDomain: "chatroom-b1dd4.firebaseapp.com",
        databaseURL: "https://chatroom-b1dd4.firebaseio.com",
        projectId: "chatroom-b1dd4",
        storageBucket: "chatroom-b1dd4.appspot.com",
        messagingSenderId: "526102862036"
    };

    firebase.initializeApp(config);

    $(".chatroom").hide();

    getStopList();


    $(".subject").on("click", function(){
        $(".subject").removeClass("active");
        $(this).addClass("active");
        selectedSubject = $(this).attr("id").split('-')[1];
        if($(".input-name").val().length>0){
            $(".start").removeClass("notactive");
        }else{
            $(".start").addClass("notactive");
        }
    });

    

    $(".left-btn").on("click", function(){
        $("#h-"+ selectedHead).removeClass("active-head");
        selectedHead--;
        if(selectedHead <= 1){
            selectedHead = 30;
        }
        $("#h-"+ selectedHead).addClass("active-head");
    });

    $(".right-btn").on("click", function(){
        $("#h-"+ selectedHead).removeClass("active-head");
        selectedHead++;
        if(selectedHead >= 30){
            selectedHead = 1;
        }
        $("#h-"+ selectedHead).addClass("active-head");
    });

    
    $(".start").on("click", function(){
        
        yourName = $(".input-name").val();
        if(!(yourName.length==0 || selectedSubject=="")){
            $(".chat-title").text(changeSubject()+"版聊天室");
            registerFireBase();
            $(".input-name").val("");
            $(".template .me .name").text(yourName);
            $(".template .me .head-img").attr("src", $("#h-"+selectedHead).attr("src"));
            $(".login").hide();
            $(".chatroom").fadeIn(600);
        }
    });

    $(".input-name").on("keyup", function(e){
        if($(".input-name").val().length>0 && selectedSubject!=""){
            $(".start").removeClass("notactive");
        }else{
            $(".start").addClass("notactive");
        }
    });

    $(".leave").on("click", function(){
        $(".chatroom").hide();
        $(".login").fadeIn(600);
        $(".msg-area").html("<br><br><br><br><br>");
        msgDbRef.off('child_added');
    });

    $(".input-chat").on("keyup", function(e){
        if($(".input-chat").val().length>0 && !isStopTalking){
            $(".send-msg").removeClass("notactive");
        }else{
            $(".send-msg").addClass("notactive");
        }
    });

    $(".send-msg").on("click", function(){
        var myMsg = $(".input-chat").val();
        if(myMsg.length ==0 || isStopTalking){
            return 0;
        }

        $(".input-chat").val("");

        var newMsgRef = msgDbRef.push();
        newMsgRef.set({
            name: yourName,
            msg: myMsg,
            head: selectedHead,
            id: blinkID
        });

    });

});


function registerFireBase(){
    msgDbRef = firebase.database().ref('/msg/'+selectedSubject);
    msgDbRef.on('child_added', function(data) {
        let addedData = data.val();
        if(addedData.name == "" || addedData.msg==""){
            return 0;
        }
        let user = "other";
        if(addedData.id == blinkID){
            user = "me";
        }
        $(".template ."+user+" .name").text(addedData.name);
        $(".template ."+user+" .head-img").attr("src", $("#h-"+addedData.head).attr("src"));
        $(".template ."+user+" .msg").text(addedData.msg);
        $(".msg-area").append('<div class="'+user+'">' + $(".template ."+user).html() + '</div>');
        if($(".msg-area > div").length>=3000){
            $(".msg-area > div")[0].remove();
        }
        let distance =  $(".msg-area")[0].scrollHeight;
        if($(".input-chat").val().length==0){
            $(".msg-area").stop().animate({scrollTop:distance}, 500, 'swing');
        }
    });

}

var stopTalkingList = {};

function getStopList(){
    let stopListDbRef = firebase.database().ref('/stop');

    stopListDbRef.on('value', function(data) {
        stopListDbRef.once("value").then(function(snapshot) {
            stopTalkingList = snapshot.val();
            checkStopTalking();
        });
        
    });
}

let isStopTalking = false;
function checkStopTalking(){
    isStopTalking = false;

    $.each(stopTalkingList, function(index, value) {
        if(value.id == blinkID){
            isStopTalking = true;
        }
    });

    if(isStopTalking){
        $(".not-stop-talking-input").hide();
        $(".stop-talking-input").show();
    }else{
        $(".not-stop-talking-input").show();
        $(".stop-talking-input").hide();
    }
    
}



function changeSubject(){
    let tempSubject ="";
    if(selectedSubject=="work"){
        tempSubject = "工作";
    }else if(selectedSubject=="interview"){
        tempSubject = "面試";
    }else if(selectedSubject=="chat"){
        tempSubject = "閒聊";
    }else if(selectedSubject=="practice"){
        tempSubject = "實習";
    }
    return tempSubject;

}


function setBlinkId(id){

    blinkID = id;

}