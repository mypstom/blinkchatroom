var msgDbRef;
//禁言列表
var stopListDbRef;
var selectedHead = 1;
var selectedSubject = "";
var yourName = "";
//管理員權限
var blinkID = "admin";
$(document).ready(function(){

    //setBlinkId() 可以更換ID

    var config = {
        apiKey: "AIzaSyAGAdDpQWt52bV6_PUgpp55aawYN0k9Jyg",
        authDomain: "chatroom-b1dd4.firebaseapp.com",
        databaseURL: "https://chatroom-b1dd4.firebaseio.com",
        projectId: "chatroom-b1dd4",
        storageBucket: "chatroom-b1dd4.appspot.com",
        messagingSenderId: "526102862036"
    };
    firebase.initializeApp(config);

    

    $(".chatroom").hide();

    $(".subject").on("click", function(){
        $(".subject").removeClass("active");
        $(this).addClass("active");
        selectedSubject = $(this).attr("id").split('-')[1];
        if($(".input-name").val().length>0){
            $(".start").removeClass("notactive");
        }else{
            $(".start").addClass("notactive");
        }
    });

    

    $(".left-btn").on("click", function(){
        $("#h-"+ selectedHead).removeClass("active-head");
        selectedHead--;
        if(selectedHead <= 0){
            selectedHead = 30;
        }
        $("#h-"+ selectedHead).addClass("active-head");
    });

    $(".right-btn").on("click", function(){
        $("#h-"+ selectedHead).removeClass("active-head");
        selectedHead++;
        if(selectedHead >= 30){
            selectedHead = 0;
        }
        $("#h-"+ selectedHead).addClass("active-head");
    });

    
    $(".start").on("click", function(){
        
        yourName = $(".input-name").val();
        if(!(yourName.length==0 || selectedSubject=="")){
            $(".chat-title").text(changeSubject()+"版聊天室");
            registerFireBase();
            $(".input-name").val("");
            $(".template .me .name").text(yourName);
            $(".template .me .head-img").attr("src", $("#h-"+selectedHead).attr("src"));
            $(".login").hide();
            $(".chatroom").fadeIn(600);
        }
    });

    $(".input-name").on("keyup", function(e){
        if($(".input-name").val().length>0 && selectedSubject!=""){
            $(".start").removeClass("notactive");
        }else{
            $(".start").addClass("notactive");
        }
    });

    $(".leave").on("click", function(){
        $(".chatroom").hide();
        $(".login").fadeIn(600);
        $(".msg-area").html("<br><br><br><br><br>");
        msgDbRef.off('child_added');
        stopListDbRef.off('child_added');
        stopListDbRef.off('child_removed');
        $(".stop-content").html("");
    });

    $(".input-chat").on("keyup", function(e){
        if($(".input-chat").val().length>0){
            $(".send-msg").removeClass("notactive");
        }else{
            $(".send-msg").addClass("notactive");
        }
    });

    $(".send-msg").on("click", function(){
        var myMsg = $(".input-chat").val();
        if(myMsg.length ==0 ){
            return 0;
        }

        $(".input-chat").val("");

        var newMsgRef = msgDbRef.push();
        newMsgRef.set({
            name: yourName,
            msg: myMsg,
            head: selectedHead,
            id: blinkID
        });

    });




    $(".stop-talking").on("click", function(){
        $(".stop-list").fadeIn();
    });

    $(".stop-list").on("click", function(){
        $(".stop-list").hide();
    });

    $(".list-body").on("click", function(e){
        e.stopPropagation();
    });

    $(".remove-history div").on("click", function(e){
        e.stopPropagation();
    });

    $(".remove-history .open-remove").on("click", function(e){
        $(this).hide();
        $(".remove-btn").show();
        e.stopPropagation();
    });

    $(".remove-btn").on("click", function(e){
        let tempCat = $(this).attr("id");
        firebase.database().ref('/msg/'+tempCat).set({});
        window.alert("已刪除"+tempCat+"紀錄");
    });

    $(".stop-send-msg").on("click", function(){
        let stopTalkingId = $(".stop-input-chat").val();
        $(".stop-input-chat").val("");
        var newStopRef = stopListDbRef.push();
        newStopRef.set({
            id: stopTalkingId
        });
    });
    

    
    


});


function registerFireBase(){
    msgDbRef = firebase.database().ref('/msg/'+selectedSubject);
    msgDbRef.on('child_added', function(data) {
        let addedData = data.val();
        if(addedData.name == "" || addedData.msg==""){
            return 0;
        }
        let user = "other";
        if(addedData.id == blinkID){
            user = "me";
        }
        $(".template ."+user+" .name").html(addedData.name + "<span class='b-id'>" +addedData.id + "</span>");
        $(".template ."+user+" .head-img").attr("src", $("#h-"+addedData.head).attr("src"));
        $(".template ."+user+" .msg").text(addedData.msg);
        $(".msg-area").append('<div class="'+user+'">' + $(".template ."+user).html() + '</div>');
        let distance =  $(".msg-area")[0].scrollHeight;
        if($(".input-chat").val().length==0){
            $(".msg-area").stop().animate({scrollTop:distance}, 500, 'swing');
        }
    });


    stopListDbRef = firebase.database().ref('/stop');
    stopListDbRef.on('child_added', function(data) {
        let addedData = data.val();
        
        
        $(".template .stop-template .stop-id").text(addedData.id);
        $(".template .stop-template .cancel-stop").attr("blink-id", addedData.id);
        
        $(".stop-content").append($(".template .stop-template")[0].outerHTML);

        let distance =  $(".stop-content").height();
        $(".stop-content").stop().animate({scrollTop:distance}, 500, 'swing');


        $(".cancel-stop").on("click", function(){
            let tempBlinkId =  $(this).attr("blink-id");
            console.log(tempBlinkId);
            var query = stopListDbRef.orderByKey();
            query.once("value").then(function(snapshot) {
                snapshot.forEach(function(childSnapshot) {
                    var pkey = childSnapshot.key; 
                    var chval = childSnapshot.val();
                    if(chval.id == tempBlinkId){
                        firebase.database().ref("/stop/"+pkey).remove();
                        return true;
                    }
    
                });
            });
        });
    });

    stopListDbRef.on('child_removed', function(data) {
        $(".stop-content").html("");
        stopListDbRef.once("value").then(function(snapshot) {
            snapshot.forEach(function(childSnapshot) {
                
                let addedData = childSnapshot.val();
                $(".template .stop-template .stop-id").text(addedData.id);
                $(".template .stop-template .cancel-stop").attr("blink-id", addedData.id);
                
                $(".stop-content").append($(".template .stop-template")[0].outerHTML);

                let distance =  $(".stop-content").height();
                $(".stop-content").stop().animate({scrollTop:distance}, 500, 'swing');


                $(".cancel-stop").on("click", function(){
                    let tempBlinkId =  $(this).attr("blink-id");
                    console.log(tempBlinkId);
                    var query = stopListDbRef.orderByKey();
                    query.once("value").then(function(snapshot) {
                        snapshot.forEach(function(childSnapshot) {
                            var pkey = childSnapshot.key; 
                            var chval = childSnapshot.val();
                            if(chval.id == tempBlinkId){
                                firebase.database().ref("/stop/"+pkey).remove();
                                return true;
                            }
            
                        });
                    });
                });

            });
        });
    });



}


function changeSubject(){
    let tempSubject ="";
    if(selectedSubject=="work"){
        tempSubject = "工作";
    }else if(selectedSubject=="interview"){
        tempSubject = "面試";
    }else if(selectedSubject=="chat"){
        tempSubject = "閒聊";
    }else if(selectedSubject=="practice"){
        tempSubject = "實習";
    }
    return tempSubject;

}